# HealthForge Coding Quiz

Hello! Thanks for taking the time to do our technical test. We know these are annoying, but we need a way to assess everyone that we employ to avoid disappointment on both sides later on.

Please follow the instructions below and when you are done, put your code into a git repo you own and send us a link to it for us to assess. GitHub or BitBucket are both free for public repos.
 
Feel free to use as much Google-Fu as you need to (that is what we do on a daily basis) - however, you need to do the test on your own. Any outside help is not in the spirit of the test and will result in seven or eight years of bad luck, depending on which is worst for you.

**Impress us with your skills and show us what you can do! This is your chance to shine and the more we see the better.**

# Pre-requesites

You will need:

- a git client
- Java 8
- Maven 3.3 or later
- NodeJS
- npm
- an IDE of your choice

Things you should search for on the web if you don't know about them already are:

- Spring Boot, Spring MVC
- FasterXML's Jackson for JSON support
- JodaTime for dates
- JUnit
- Swagger
- OpenID Connect

We haven't tried this on Windows, but it should work and it definitely works on MacOS or Linux.

# Getting Started

Clone the repo somewhere locally.

The source code will initially not build, but that is part of the test... so don't worry about that just yet.

The code implements a REST API for managing patients, clinicians and orders. It has documentation provided by Swagger that allows you to play with the API by going to [http://localhost:8080/](http://localhost:8080/) once the code compiles and runs.

Later on in the test, when you need to run the app, you can do this with:

    mvn spring-boot:run

And to run integration tests:

    mvn test

# Question 1

Currently the patient API only allows searching by date of birth.

**We would like you to add a new feature to the Patient API's `getAll` method that
 allows searching by `postcode`. The user would send a request as follows to search by postcode:**

    http://localhost:8080/api/patient?postcode=90210

And the expected response would be:

    {
      "totalItems": 1,
      "items": [
        {
          "id": "...",
          "firstName": "Patient",
          "lastName": "Epsilon",
          ...
        }
      ]
    }

#### Addresses

The addresses in our patient dataset have the following formats:

    Sumo Informática S.A.
    Calle 39 No 1540
    B1000TBU San Sebastian       Postal code, Municipality
    Argentina

    LANCE MULTILINGUA-EBEL
    FOREIGN TEACHERS' MAILBOX
    LIANGANG MIDDLE SCHOOL
    LOUDI
    HUNAN
    417009                        Postal code
    People's Republic of China

    вул. Цэнтральная, д. 20       Streetname, number, apartment/room
    в. Караліставічы              Village (in rural areas when different from post office)
    223016, п/а Новы Двор         Postal code, post office (in rural areas) or city/town
    Мінскага р-на.                Raion
    Мінскай вобл.                 Region
    Belarus                       Country

    日本国 〒112-0001              Country, 〒Postal Code
    東京都文京区白山4丁目3-2         Prefecture, City, District, Chōme, Banchi, Building Number and Name, Room Number
    Japan

    90210 Palm Drive              Number and street name
    Greenville                    Town
    CA 90210                      State Code, Postal Code
    USA                           Country

#### Some tips

- There is an integration test in the code already, that will test if you got it right...
- We like object oriented programming and we hope that you do too!
- We like unit tests and integration tests
- This is a Spring project, so EJBs would be a bit out of place, as would custom singletons
- The patients will have random UUIDs each time you run the app, as they are loaded from file at startup

And finally:

- The code must build and function correctly, even after your additions, don't mess with things outside of the Patient API, unless it adds value
- Someone has to look at the code you add, so help them out, just like you would day-to-day


# Question 2

We would like you to create an application that uses the API described below and displays a list of patients.

You can use any frameworks and technologies you like.

The patient list should have the following features:

- Display a list of patients with header columns
- Clicking on a patient in the list should display that patient's details

No authentication is required, so use the `patient` resource.

**Your app should be run on `http://localhost:4444` to avoid CORS issues with our API.**

It would be nice to see paging, sorting and filtering, as well as some styling.

It should look something like this, but feel free to adjust if you can make it better:

![alt text](patient_list.png "Patient List")

## API

You can view documentation for the API at:

https://api.interview.healthforge.io

There are two resources:

* `patient` - no authentication required
* `patient-secure` - requires a bearer token

Use the `Authorize` link at the top of the Swagger UI and log in with the following credentials:

- Username: `interview`
- Password: `Interview01`

You should now be able to access the secure patient resources.

**For this questions, remember to use the `patient` resource that doesn't require authentication.**

# Question 3

Create a copy of your application in Question 2 and modify it to use the `patient-secure` resource. The user of the application is required to supply login details (username=`interview` and password=`Interview01`).

![alt text](with_auth.png "With authentication")

You will need to use our authentication server (OpenID Connect) with the following details:

* token url: `https://auth.healthforge.io/auth/realms/interview/protocol/openid-connect/token`
* client id: `interview`
* no client secret is required as it is public
* allowed origins: `http://localhost:4444`
* you must use the OAuth (and OpenID Connect) hybrid flow

**Your app should be run on `http://localhost:4444` to avoid CORS issues with our authentication server.**

We recommend that you use the Keycloak Javascript Adapter (`npm install keycloak-js`) to interact with the our authentication server:

https://keycloak.gitbooks.io/securing-client-applications-guide/content/topics/oidc/javascript-adapter.html

You will need to supply `keycloak.json`:

```
{
  "realm": "interview",
  "auth-server-url": "https://auth.healthforge.io/auth",
  "ssl-required": "external",
  "resource": "interview",
  "public-client": true
}
```

Or you can configure the client adapter in code:

```
var keycloak = Keycloak({
    url: 'https://auth.healthforge.io/auth',
    realm: 'interview',
    clientId: 'interview'
});
```

The OpenID Connect client that you will be using is public, so it does not require a client secret to be set.

Here is an example of authenticating with a very simple static HTML page served locally:

```
<html>
<head>
    <script src="https://auth.healthforge.io/auth/js/keycloak.js"></script>
    <script>
    var keycloak = Keycloak({
      url: 'https://auth.healthforge.io/auth',
      realm: 'interview',
      clientId: 'interview'
    });
    keycloak.init({ onLoad: 'login-required' });
    </script>
</head>
<body>
  <h1>Well done, you successfully authenticated!</h1>
  <p><a href="https://auth.healthforge.io/auth/realms/interview/protocol/openid-connect/logout?redirect_uri=http%3A%2F%2Flocalhost%3A4444">Logout</a></p>
</body>
</html>
```

You can test it by creating `index.html` from the snippet above and serving it with:

```
sudo npm install http-server -g
http-server -p 4444 --cors
```

Browse to `http://localhost:4444/` and enter the username `interview` and password `Interview01`.

Also, an example of how to do this with Angular can be found here:

https://github.com/keycloak/keycloak/tree/master/examples/demo-template/angular-product-app/src/main/webapp

# And finally...

Good luck and have fun!